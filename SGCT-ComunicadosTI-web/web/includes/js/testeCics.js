$(document).ready(function(){
	TESTE.definirCamposObrigatorios();
	TESTE.init();
});

var TESTE = {
	
	init: function() {		
		$(".numerico-mask").mask("9?9999999", {placeholder : ""});
		$("#btnEnviar").off('click').on('click', TESTE.enviar);
		REBNNT.padronizarBotoes('input:button', 137);
	},
	
	definirCamposObrigatorios: function(){
		$(".validation").each(function(){
			$(this).attr("label-validation", "label-" + $(this).attr("id"));
		});
	},
	
	enviar: function() {
		REBNNT.processando(true);
		var fieldsToValidation = $("*.validation");
		var blocoMensagem = "bloco-mensagem";
		if($.validaCamposObrigatoriosCobertura(blocoMensagem, {"fieldsToValidation": fieldsToValidation})) {			
			REBNNT.processando(false);
			return;
		}
		$("#form-teste-cics").get(0).submit();
	},
};