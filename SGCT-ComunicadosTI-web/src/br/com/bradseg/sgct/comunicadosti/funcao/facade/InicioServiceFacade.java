package br.com.bradseg.sgct.comunicadosti.funcao.facade;

/**
 * Descri��o do DAO da Funcionalidade
 * 
 * @author Bradesco Seguros
 */
public interface InicioServiceFacade {

	/**
	 * Descri��o do m�todo
	 * 
	 * @param nome do argumento
	 */
	public String consultarSaudacao(String nome);

}
