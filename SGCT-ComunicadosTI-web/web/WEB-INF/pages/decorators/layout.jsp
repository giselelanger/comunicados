<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="page" uri="http://www.opensymphony.com/sitemesh/page" %>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title><decorator:title default="REBN-Nautico"/></title>
	
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	
	
	<!-- CSS ENV -->
	<link href="<s:url value="/includes/css/3rdparty/bootstrap.min.css"/>" rel="stylesheet" type="text/css" />
	<link href="<s:url value="/includes/css/3rdparty/bootstrap-theme.min.css"/>" rel="stylesheet" type="text/css" />
	<link href="<s:url value="/includes/css/3rdparty/bootstrap-datepicker3.min.css"/>" rel="stylesheet" type="text/css" />
	<link href="<s:url value="/includes/css/intranet/nautico.intranet.css"/>" rel="stylesheet" type="text/css" />
	<link href="<s:url value="/includes/css/3rdparty/jquery.dataTables.min.css"/>" rel="stylesheet" type="text/css" />
	<link href="<s:url value="/includes/css/3rdparty/dataTables.bootstrap.min.css"/>" rel="stylesheet" type="text/css" />
	<link href="<s:url value="/includes/css/intranet/topo.css"/>" rel="stylesheet" type="text/css" />
	<link href="<s:url value="/includes/css/intranet/vendorOverwrite.css"/>" rel="stylesheet" type="text/css" />
	<link href="<s:url value="/includes/css/intranet/icons.css"/>" rel="stylesheet" type="text/css" />
	<link href="<s:url value="/includes/css/intranet/mainStyle.css"/>" rel="stylesheet" type="text/css" />
	<link href="<s:url value="/includes/css/3rdparty/bootstrap-switch.min.css"/>" rel="stylesheet" type="text/css" />
	
	<!-- JS ENV -->
	<script type="text/javascript" charset="utf-8" src="<s:url value='/includes/js/3rdparty/jquery-1.11.3.min.js'/>" ></script>
	<script type="text/javascript" src="<s:url value='/includes/js/3rdparty/jquery.maskedinput.min.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/3rdparty/jquery.maskMoney.min.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/3rdparty/jQuery.MultiFile.min.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/3rdparty/bootstrap.min.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/3rdparty/bootstrap-datepicker.min.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/3rdparty/bootstrap-datepicker.pt-BR.min.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/3rdparty/handlebars-v4.0.2.min.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/3rdparty/handlebers-helper.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/comum.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/3rdparty/jquery.dataTables.min.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/3rdparty/moment.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/3rdparty/dataTables.bootstrap.min.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/3rdparty/bootstrap-switch.min.js'/>"></script>
    
    <script>
	    $(document).ready(function(){
	        REBNNT.init();
	    });
    </script>
    
	<!-- HEAD -->
	<decorator:head />
</head>
<body>
	<div id="esqueleto" class="container-fluid" style="padding-top: 20px">
		<div class="row">
			<div class="container menu">
				<div class="row">
					
					<div class="col-xs-6">
								<img
									src='<s:url value="/includes/img/logo-bradesco.png"/>'
									width="35px" height="35px"> <img
									src='<s:url value="/includes/img/titulo.png"/>'  height="35px">
					</div>
					<div class="col-xs-3 alignRight barra-login font11px">
						<span class="formIcons userIcon">-</span>
						<span class="fontBlue topoLogin">
								Usu&aacute;rio n&atilde;o logado
						</span>
						<br/>
						<h6>Vers�o 1</h6>
					</div>
				</div>
			   <br>
			</div>
			<div class="col-xs-12">
				<div class="container">
					<decorator:body />
				</div>
			</div>
		</div>
		
	</div>
	
	<div id="modalLoading" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h4 class="modal-title">Em processamento</h4>
	      </div>
	      <div class="modal-body">
	        <p style="font-size: 14px;"><img src="<s:url value="/includes/img/loading.gif" />" class="rebnnt-icon-loader" />&nbsp;&nbsp;Por favor, aguarde...</p>
	      </div>
	    </div>
	  </div>
	</div>
</body>
</html>
