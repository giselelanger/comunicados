var REBNNT = {

		appcontext: "/SGCT-ComunicadosTI",
		
		SUCCESS_TEXT_STATUS      : "success",
		TIMEOUT_TEXT_STATUS      : "timeout",
		PARSER_ERROR_TEXT_STATUS : "parsererror",	

		/**
		 * Adiciona o contexto da aplicacao na URI passada como param.
		 */		
		getURI: function(path){
			return REBNNT["appcontext"] + path;
		},

		/**
		 * Apresentacao com modal para operacoes de Confirmacao
		 */
		confirm: function(mensagem, fnCallback){
			$("#modalConfirmacao").remove();

			var template = Handlebars.compile($('#template-confirm').html());
			$("body").append(template({'mensagem': mensagem}));
			$("#modalConfirmacao").modal();

			$(".btn-confirmacao-sim").off('click').on('click', function(){
				fnCallback.apply();
			});
		},


		/**
		 * Apresentacao com modal para operacoes de Confirmacao
		 */
		confirmacao: function(mensagem){
			$("#modalConfirmacao").remove();

			var template = Handlebars.compile($('#template-confirm').html());
			$("body").append(template({'mensagem': mensagem}));
			$("#modalConfirmacao").modal();

			$(".btn-confirmacao-sim").off('click').on('click', function(){
				return true;
			});

			$(".btn-confirmacao-nao").off('click').on('click', function(){
				return false;
			});
		},

		/*
		 * REJEITAR COTAÇÃO
		 * 
		 */


		limparBlocoMsgValidacao: function(){
			var passoRejeicao = parseInt($(this).attr("data-rejeitar")); 
			
			 if ($('#bloco-mensagem-rejeicao-' + passoRejeicao).size() > 0) {
					$('#bloco-mensagem-rejeicao-' + passoRejeicao).html('');
					$('#bloco-mensagem-rejeicao-' + passoRejeicao).html('&nbsp;');
				}	
		},

		confirmRejeicao: function(passoRejeitar, mensagem, fnCallback){
			$("#modalConfirmacao").remove();

			var template = Handlebars.compile($('#template-confirm').html());
			$("body").append(template({'mensagem': mensagem}));
			$("#modalConfirmacao").modal();

			$(".btn-confirmacao-sim").off('click').on('click', function(){
				fnCallback.apply();
			});
			$(".btn-confirmacao-nao").off('click').on('click', function(){
				$("#div-obsRejeitacao-" + passoRejeitar).hide();
				$("#motivoRejeicao-" + passoRejeitar).val('');
				 if ($('#bloco-mensagem-rejeicao-' + passoRejeitar).size() > 0) {
					$('#bloco-mensagem-rejeicao-' + passoRejeitar).html('');
					$('#bloco-mensagem-rejeicao-' + passoRejeitar).html('&nbsp;');
				 }	
			});
		},

		rejeitarCotacao: function (){
			var passoRejeitar = parseInt($(this).attr("data-rejeitar")); 
			var masgAlert = "Deseja cancelar esta Cota&ccedil;&atilde;o?";
			//$(".alert-danger").text(masgAlert);
			REBNNT.confirmRejeicao(passoRejeitar,masgAlert, function(){
				 if ($('#bloco-mensagem-rejeicao-' + passoRejeitar).size() > 0) {
						$('#bloco-mensagem-rejeicao-' + passoRejeitar).html('');
						$('#bloco-mensagem-rejeicao-' + passoRejeitar).html('&nbsp;');
					}	
				$("#div-obsRejeitacao-" + passoRejeitar).show();
				$('#motivoRejeicao-' + passoRejeitar).focus();
				$("html, body").stop().animate({scrollTop:0}, 1000, 'swing');
			});
		},
		salvarRejeitarCotacao: function (){
			var passoRejeitar = parseInt($(this).attr("data-rejeitar"));
			if($("#motivoRejeicao-" + passoRejeitar).val() != ''){
				REBNNT.executaRejeitarCotacao(passoRejeitar);
				
			}else{
				$('#bloco-mensagem-rejeicao-'+ passoRejeitar).html('O campo Motivo &eacute; obrigat&oacute;rio.');	
				document.getElementById("bloco-mensagem-rejeicao-" + passoRejeitar).focus();
				$('#bloco-mensagem-rejeicao-'+ passoRejeitar).focusin();
				return;
				
			}
		},
		cancelarRejeitarCotacao: function (){
			var passoRejeitar = parseInt($(this).attr("data-rejeitar")); 
				$("#div-obsRejeitacao-" + passoRejeitar).hide();
				$("#motivoRejeicao-" + passoRejeitar).val('');
				 if ($('#bloco-mensagem-rejeicao-' + passoRejeitar).size() > 0) {
					$('#bloco-mensagem-rejeicao-' + passoRejeitar).html('');
					$('#bloco-mensagem-rejeicao-' + passoRejeitar).html('&nbsp;');
				 }	
		},
		
		executaRejeitarCotacao: function (passoRejeitar){

				var params = {
						"cotacaoVo.numeroCotacao" : $("#numeroCotacao").val(),
						"cotacaoVo.motivoRejeicao" : $("#motivoRejeicao-" + passoRejeitar).val()
					};
				$.ajax({
					
		            url:      REBNNT.getURI("/json/rejeitarCotacao.ajax"),
		            type:     "GET",
		            data:     params,
		            dataType: "json",
		            
		            beforeSend : function() {
		            	REBNNT.processando(true);
					},
		            success: function(data){
		            	$('.btn-rejeitado').attr('disabled', true);
		            	$(".btn-rejeitado").hide();
		            	REBNNT.processando(false);
		            },
		            complete : function() {
		            	REBNNT.voltarResumo(passoRejeitar);
					}
		         });
				
		},	

		verificaCotacaoRejeitada: function (){
			if($("#numeroCotacao").val() != ''){
			var params = {
					"cotacaoVo.numeroCotacao" : $("#numeroCotacao").val()
				};
			$.ajax({
				
	            url:      REBNNT.getURI("/json/verificaCotacaoRejeitada.ajax"),
	            type:     "GET",
	            data:     params,
	            dataType: "json",
	            
	            beforeSend : function() {
	            	REBNNT.processando(true);
				},
	            success: function(data){
	            	if(data.permiteRejeicao){
	            		$('.btn-rejeitado').attr('disabled', true);
	            		$(".btn-rejeitado").hide();
	            		//REBNNT.voltarResumoCotacaoRejeitada();
	            	}
	            	REBNNT.processando(false);
	            },
	            complete : function(data) {
	            	if(data.permiteRejeicao){
	            		
	            	}
				}
	         });
			}
	},	
		voltarResumo : function(passoRejeitar) {
			if(passoRejeitar == 10){
				document.getElementById('voltarResumoCotacaoRejeitada').click();
			}else{
				document.getElementById('voltarResumo-' + passoRejeitar).click();
			}
			
			REBNNT.processando(true);
		},

		voltarResumoCotacaoRejeitada : function() {
			document.getElementById('voltarResumoCotacaoRejeitada').click();
			REBNNT.processando(true);
		},

		/*
		 * FIM REJEITAR COTAÇÃO
		 * 
		 */
		
		/**
		 * Apresentacao com modal para operacoes de Confirmacao
		 */
		confirmRejeicao_old: function(mensagem, fnCallback){
			$("#modalConfirmaRejeicao").remove();

			var template = Handlebars.compile($('#template-confirm-rejeicao').html());
			$("body").append(template({'mensagem': mensagem}));
			$("#modalConfirmaRejeicao").modal();

			$(".btn-confirmacao-sim").off('click').on('click', function(){
				fnCallback.apply();
			});
		},
		
		/**
		 * Apresentacao com modal para operacoes com erro
		 */
		modalErro: function(mensagem, mensagemDetalhe, fnCallback){
			$("#modalErro").remove();

			var template = Handlebars.compile($('#template-erro').html());
			$("body").append(template({'mensagem': mensagem}));
			$("#modalErro").modal();

			$(".btn-detalhe").off('click').on('click', function(){
				REBNNT.alertErro(mensagemDetalhe, function(){	});
			});
		},

		/**
		 * Apresentacao com modal para operacoes de Alerta
		 */
		alert: function(mensagem, fnCallback){
			$("#modalAlerta").remove();

			var template = Handlebars.compile($('#template-alert').html());
			$("body").append(template({'mensagem': mensagem}));
			$("#modalAlerta").modal();

			$(".btn-ok").off('click').on('click', function(){
				fnCallback.apply();
			});
		},
		
		/**
		 * Apresentacao com modal para operacoes de Alerta de erro
		 */
		alertErro: function(mensagem, fnCallback){
			$("#modalAlertaErro").remove();

			var template = Handlebars.compile($('#template-alert-erro').html());
			$("body").append(template({'mensagem': mensagem}));
			$("#modalAlertaErro").modal();

			$(".btn-ok").off('click').on('click', function(){
				fnCallback.apply();
			});
		},

		/**
		 * Apresentacao com modal padrão
		 */
		modalPadrao: function(mensagem, fnCallback){
			$("#modalPadrao").remove();

			var template = Handlebars.compile($('#template-modalPadrao').html());
			$("body").append(template({'texto': mensagem}));
			$("html, body").stop().animate({scrollTop:0}, 1000, 'swing');
			$("#modalPadrao").modal();

			$(".btn-ok").off('click').on('click', function(){
				fnCallback.apply();
			});
		},

		
		
		processando : function(exibir) {
			if (exibir) {
				$('#modalLoading').on('hide.bs.modal', function(e){
					return false;
				});
				$('#modalLoading').on('show.bs.modal', function () {
					$('.modal-content').css('margin-top',$( window ).height()*0.25);
				});
				$('#modalLoading').modal();
			} else {
				$('#modalLoading').off('hide.bs.modal').on('hide.bs.modal', function(e){
					$(this).removeData('bs.modal');
				});
				$('#modalLoading').modal('hide');
			}
		},

		/**
		 * Inicializa um comportamento padrao para todas as funcionalidades.
		 * Este metodo deve ser chamado do template comum a todas as paginas.
		 */
		init: function() {
			REBNNT.initAjaxSetup();
			REBNNT.fixMaxLength();
			REBNNT.fixJSON();
			$(".bootstrap-switch-on").bootstrapSwitch();
			$('[data-toggle="tooltip"]').tooltip(); 
		},
		
		inputTypeFile: function(){ 
			$("#tipoArquivo").change(function(){
				if($("#tipoArquivo").val() == ""){					
					$(".jfilestyle").prop("disabled", "disabled");
					$("#texto").css("color", "#D3D3D3");
				} else {
					$(".jfilestyle").prop("disabled", "");
					$("#texto").css("color", "black");
				}				
			});
		},

		isExtensaoValidaUploadDocumento : function(extensao) {
	    	extensoesValidas = new Array('doc','docx','xls','xlsx','pdf','jpg','jpeg' );
			extensaoValida = false;
			for(var i = 0; i <= extensoesValidas.length; i++){
		        if(extensoesValidas[i] == extensao){
		            extensaoValida = true;
		            break;
		        }
		    }
			return extensaoValida;
		},	
		
		/**
		 * Configuracao global das requisicoes AJAX. 
		 */
		initAjaxSetup: function(){
			$.ajaxSetup({
				global:  true,
				cache:   false,
				async:   true,
				timeout: 120000000, //(milliseconds) 

				error: function(xhr, textStatus, errorThrown) {

					if(textStatus == REBNNT.TIMEOUT_TEXT_STATUS) {
						alert("O servidor n&atilde;o conseguiu responder a solicita&ccedil;&atilde;o a tempo.");
						return;
					}

					if(textStatus == REBNNT.PARSER_ERROR_TEXT_STATUS) {
						var pos = 1;
						var ini = xhr.responseText.indexOf("INICIO DETALHE ERRO:");
						var fim = xhr.responseText.indexOf("FIM DETALHE ERRO:");
						if (ini>0) {
							pos_ini = --ini;
							pos_fim = --fim; 
						}
						REBNNT.modalErro("OCORREU UM ERRO INTERNO. TENTE NOVAMENTE EM ALGUNS MINUTOS.<br/><br/>" + 
								"Caso o erro persista, favor entrar em contato com o analista responsável e informar o detalhe.", 
								xhr.responseText.substring(pos_ini, pos_ini+8500), function(){
						});
						return;
					}

					switch(xhr.status) {
					case 401:
						break;

					case 403:
						REBNNT.alert("Permiss&atilde;o negada para acessar o recurso solicitado.");
						break;

					default:
						var errorTicket = xhr.getResponseHeader("errorTicket");
					if (errorTicket != null && errorTicket != ""){
						alert("Ocorreu um erro na aplica&ccedil;&atilde;o. Favor informar o c&oacute;digo <br/><br/> "+ errorTicket);
					}else{
						alert("Ocorreu um erro na aplica&ccedil;&atilde;o.");
					}
					break;
					}
				}
			});
		},
		
		fixDatePicker : function (){
			var data = $(this).val().replace(/\/|\./g,""),
			ano = parseInt(data.length === 5 ? data.substring(4,5) : data.substring(4,6)),
			mes = parseInt(data.substring(2,4)),
			dia = parseInt(data.substring(0,2)),
			anoCompleto = (parseInt( ( new Date().getFullYear().toString().substring(0,2).concat("00") ) ) + ano),
			arr = $(this).val().split("/");
			if(arr[2].length == 1 && arr[2] == "0"){
				var dataAtual = new Date();
			    var diaAtual = dataAtual.getDate();
			    if (diaAtual.toString().length == 1)
			      diaAtual = "0"+diaAtual;
			    var mesAtual = dataAtual.getMonth()+1;
			    if (mesAtual.toString().length == 1)
			      mesAtual = "0"+mesAtual;
			    var anoAtual = dataAtual.getFullYear();  
			    $(this).val(diaAtual+"/"+mesAtual+"/"+anoAtual);
			    $(this).datepicker('update', new Date(anoAtual,(mesAtual-1), diaAtual));
			    return;
			}
			// 6 -> [20]dia [08]mes [16]ano / 5 -> [20]dia [8]mes [16]ano
			if(data.length === 6 || data.length === 5){	
				$(this).datepicker('update', new Date(anoCompleto,(mes-1), dia));
			} else if(data.length === 7){
				var anoAtual = new Date().getFullYear();
				$(this).datepicker('update', new Date(anoAtual,(mes-1),dia));
			}
		},
		
		fixMaxLength: function() {
			$("textarea[maxlength]").bind('input propertychange', function() {
		        var maxLength = $(this).attr('maxlength');  
		        if ($(this).val().length > maxLength) {  
		            $(this).val($(this).val().substring(0, maxLength));  
		        }  
		    })
		},
		
		fixJSON : function() {
			if (!window.JSON) {
			  window.JSON = {
			    parse: function(sJSON) { return eval('(' + sJSON + ')'); },
			    stringify: (function () {
			      var toString = Object.prototype.toString;
			      var isArray = Array.isArray || function (a) { return toString.call(a) === '[object Array]'; };
			      var escMap = {'"': '\\"', '\\': '\\\\', '\b': '\\b', '\f': '\\f', '\n': '\\n', '\r': '\\r', '\t': '\\t'};
			      var escFunc = function (m) { return escMap[m] || '\\u' + (m.charCodeAt(0) + 0x10000).toString(16).substr(1); };
			      var escRE = /[\\"\u0000-\u001F\u2028\u2029]/g;
			      return function stringify(value) {
			        if (value == null) {
			          return 'null';
			        } else if (typeof value === 'number') {
			          return isFinite(value) ? value.toString() : 'null';
			        } else if (typeof value === 'boolean') {
			          return value.toString();
			        } else if (typeof value === 'object') {
			          if (typeof value.toJSON === 'function') {
			            return stringify(value.toJSON());
			          } else if (isArray(value)) {
			            var res = '[';
			            for (var i = 0; i < value.length; i++)
			              res += (i ? ', ' : '') + stringify(value[i]);
			            return res + ']';
			          } else if (toString.call(value) === '[object Object]') {
			            var tmp = [];
			            for (var k in value) {
			            if (value.hasOwnProperty(k))
			                tmp.push(stringify(k) + ': ' + stringify(value[k]));
			            }
			             return '{' + tmp.join(', ') + '}';
			          }
			        }
			        return '"' + value.toString().replace(escRE, escFunc) + '"';
			      };
			    })()
			  };
			}
		},
		
		padronizarBotoes : function(elem, tamanho) {
			elem = elem || 'button, input:button';
			var maxWidth = tamanho || Math.max.apply( null, 
				$( elem )
					.map(function () {
					    return $( this ).outerWidth( true );
					})
					.get() );
			$( elem ).css('width', maxWidth);
		},
		
		validarCPF : function(strCPF) {
			var cpfValido = true;
			if(strCPF != ''){
				strCPF = strCPF.replace('-','');
				strCPF = strCPF.replace('.','');
				strCPF = strCPF.replace('.','');
				
			    var soma = 0;
			    var resto = 0;
			    Soma = 0;
				//if (strCPF == "00000000000") cpfValido = false;
				 if (strCPF == "00000000000" ||  strCPF == "11111111111" ||  strCPF == "22222222222" || strCPF == "33333333333" ||
						 strCPF == "44444444444" || strCPF == "55555555555" || strCPF == "66666666666" || strCPF == "77777777777" ||
						 strCPF == "88888888888" || strCPF == "99999999999") cpfValido = false;
			    
				for (var i=1; i<=9; i++) soma = soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
				resto = (soma * 10) % 11;
				
			    if ((resto == 10) || (resto == 11))  resto = 0;
			    if (resto != parseInt(strCPF.substring(9, 10))) cpfValido = false;
				
			    soma = 0;
			    for (var i = 1; i <= 10; i++) soma = soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
			    resto = (soma * 10) % 11;
				
			    if ((resto == 10) || (resto == 11))  resto = 0;
			    if (resto != parseInt(strCPF.substring(10, 11) ) ) cpfValido = false;
			}
	    	return cpfValido;
		},
		
		
		validarCNPJ : function(cnpj) {
			valido = true;
		    cnpj = cnpj.replace(/[^\d]+/g,'');
		    if(cnpj != '') {
			    // Elimina CNPJs invalidos conhecidos
			    if (cnpj.length != 14 ||
			    	cnpj == "00000000000000" || 
			        cnpj == "11111111111111" || 
			        cnpj == "22222222222222" || 
			        cnpj == "33333333333333" || 
			        cnpj == "44444444444444" || 
			        cnpj == "55555555555555" || 
			        cnpj == "66666666666666" || 
			        cnpj == "77777777777777" || 
			        cnpj == "88888888888888" || 
			        cnpj == "99999999999999") {
			        valido = false;
			    } else {     
				    // Valida DVs
				    tamanho = cnpj.length - 2;
				    numeros = cnpj.substring(0,tamanho);
				    digitos = cnpj.substring(tamanho);
				    soma = 0;
				    pos = tamanho - 7;
				    for (i = tamanho; i >= 1; i--) {
				      soma += numeros.charAt(tamanho - i) * pos--;
				      if (pos < 2)
				            pos = 9;
				    }
				    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
				    if (resultado != digitos.charAt(0)) {
				        valido = false;
				    } else {     
					    tamanho = tamanho + 1;
					    numeros = cnpj.substring(0,tamanho);
					    soma = 0;
					    pos = tamanho - 7;
					    for (i = tamanho; i >= 1; i--) {
					      soma += numeros.charAt(tamanho - i) * pos--;
					      if (pos < 2)
					            pos = 9;
					    }
					    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
					    if (resultado != digitos.charAt(1))
					    	valido = false;
				    }    
			    }    
		    }
		    return valido;
		},
		
		retiraEspeciaisAcentos : function(palavra) {
			var semAcentos = retiraAcentosBase(palavra);
			return retiraEspeciaisBase(semAcentos);
		},

		retiraEspeciais : function(palavra) {
			return retiraEspeciaisBase(semAcentos);
		},

		retiraAcentos : function(palavra) {
			return retiraAcentosBase(palavra);
		},

		inIframe : function() {
			 try {
			     return window.self !== window.top;
			 } catch (e) {
				 return true;
			 }
		},		

		limparCampoPorId : function(idCampo) {
			$('#'+idCampo).val('');
		},

		preencherCampoPorId : function(idCampo, valor) {
			$('#'+idCampo).val(valor);
		},
};



function retiraAcentosBase( newStringComAcento ) {
	  var string = newStringComAcento;
	  var mapaAcentosHex = {
			  a : /[\xE0-\xE6]/g,
			  A : /[\xC0-\xC6]/g,
			  e : /[\xE8-\xEB]/g,
			  E : /[\xC8-\xCB]/g,
			  i : /[\xEC-\xEF]/g,
			  I : /[\xCC-\xCF]/g,
			  o : /[\xF2-\xF6]/g,
			  O : /[\xD2-\xD6]/g,
			  u : /[\xF9-\xFC]/g,
			  U : /[\xD9-\xDC]/g,
			  c : /\xE7/g,
			  C : /\xC7/g,
			  n : /\xF1/g,
			  N : /\xD1/g,
			  };

		for ( var letra in mapaAcentosHex ) {
			var expressaoRegular = mapaAcentosHex[letra];
			string = string.replace( expressaoRegular, letra );
		}

		return string;
	}

function retiraEspeciaisBase(palavra) {
    var comAcento = '$#@*¨~`´';
    var semAcento = '';
    var nova = '';
    var desconsiderar = '()<>[]{}?.*+';
    
    for(var i = 0; i < palavra.length; i++) {
    	var letra = palavra.substr(i,1);
    	
    	var isDesconsiderar = desconsiderar.indexOf(letra)!=-1;
    	
	    if (!isDesconsiderar && comAcento.search(letra)>=0) {
	    	  nova += semAcento.substr(comAcento.search(letra),1);
 	    } else {
 	    	  nova += letra;
	    }
    }
    return nova;
}

function isDesconsiderar(palavra) {
	
	return false;
}

//##############################################################
//### JQUERY PLUGINS
//##############################################################

/**
 *  Substitui o conteudo do elemento por uma imagem de "carregando".
 */
jQuery.fn.showAjaxLoader = function(id) {
	$(this).hide();


	if(id == 'center') {
		css  = 'rebnnt-icon-loader-center';
		//$(this).after("<img src=" + REBNNT.getURI('/includes/img/intranet/boat.gif') + " class=" + css + ">");
	} else {
		css  = 'rebnnt-icon-loader';
	}
	$(this).after("<img src=" + REBNNT.getURI('/includes/img/intranet/ajax-loader.gif') + " class=" + css + ">");
	return $(this);
};

/**
 * Substitui a imagem de carregando pelo elemento original.
 */
jQuery.fn.hideAjaxLoader = function(id) {
	if(id == 'center') {
		$("img.rebnnt-icon-loader").remove();
	} else {
		$(this).show();
		$(this).next("img.rebnnt-icon-loader").remove();
	}
	return $(this);
};

/**
 * Transforma uma lista de objetos em options e adiciona no combo informado.
 * Ex: $("#idDoMeuCombo").addOptions(listaObjetos, propriedadeValue, propriedadeLabel, valueSelecionadoPorPadrao)
 */
jQuery.fn.addOptions = function(list, value, label, valueSelected) {
	var elSelect = this;
	var html = '';
	$(list).each(function(){
		var v = this[value];
		var l = this[label];

		html += "<option value='" + v + "' " + (valueSelected != undefined && valueSelected == v ? "selected='selected'" : "") + ">" + l + "</option>";
	});
	elSelect.append(html);
	return $(this);	
};

/**
 * Adiciona um option ao combo informado.
 * Ex: $("#idDoMeuCombo").addOption(propriedadeValue, propriedadeLabel, valueSelecionadoPorPadrao, index)
 */
jQuery.fn.addOption = function(value, label, valueSelected, index) {
	var option = $(document.createElement("option"));
	option.attr("value", value);
	option.html(label);

	if (valueSelected != undefined && valueSelected){
		option.attr("selected", "selected");
	}

	if(index != undefined) {
		$(this).find("option:nth-child(" + (index+1) + ")").before(option);
	} else {
		this.append(option);	
	}
	return $(this);
};

/**
 * 
 */
jQuery.removeMensagemCamposObrigatorios = function() {
	if ($('.blocoMensagemValidacao').size() > 0) {
		$('.blocoMensagemValidacao').html('');
		$('.blocoMensagemValidacao').html('&nbsp;');
	}	
	$(".redBorder").removeClass("redBorder");
	$('.naoPreenchido').removeClass('naoPreenchido');
	
};

/**
 * 
 */
jQuery.validaCamposObrigatorios = function(target, options) {
	var defaults = {
			fieldsToValidation : undefined,
			validationComplete : undefined,
			validateInvisibleFields: false,
			validateDisabledFields: false
	};
	var settings = $.extend( {}, defaults, options);

	var retorno = false;

	var fieldsWithValidationError = [];
	var callbackEachFieldValidation = function(){
		var idLabel = $(this).attr('label-validation');
		if ((settings.validateInvisibleFields || $(this).is(":visible")) 
				&& !$(this).hasClass("naoValida") 
				&& (settings.validateDisabledFields || !$(this).is(":disabled")) 
				&& (($(this).val() == undefined) || ($(this).val() == null) || ($.trim($(this).val()) == ''))) {
			idLabel = $(this).attr('label-validation');
			$('#'+idLabel).addClass('naoPreenchido');
			if(idLabel == "label-isBasica"){
				$("#isBasica").addClass("redBorder");
			} else if(idLabel == "label-desconto-fidelidade"){
				$("#tipoSeguro").addClass("redBorder");
			} else if(idLabel == "label-tempoExperiencia"){
				$("#tempoExperiencia").addClass("redBorder");
			}else if(idLabel == "label-houveAlteracaoEmbarcacao"){
				//$("#perguntaAltEmbcao209Sim").addClass("redBorder");
				//$("#perguntaAltEmbcao209Nao").addClass("redBorder");
			}
			var proxID =$('#'+idLabel).next().attr('id');
			$("#" + proxID).addClass("redBorder");						
			retorno = true;
			fieldsWithValidationError.push($(this));
		}
		$(this).on('focus', function() {
			$.removeMensagemCamposObrigatorios();
		});
	};

	var fieldsToValidate = null;
	if(settings.fieldsToValidation == undefined){
		fieldsToValidate = $('.validation');
	} else {
		fieldsToValidate = settings.fieldsToValidation;
	}

	fieldsToValidate.each(callbackEachFieldValidation);

	if ((retorno) && ($('#'+target).size() > 0)) {
		$('#'+target).html('Dados obrigat&oacute;rios n&atilde;o foram preenchidos.');		
	}

	if(settings.validationComplete != null) {
		var validationReturn = {
				'hasErrorValidation': retorno,
				'fieldsWithValidationError': fieldsWithValidationError,
				'optionsValidation': settings
		};
		settings.validationComplete(validationReturn);
		return validationReturn;
	} else {
		return retorno;
	}
};

/**
 * 
 */
jQuery.validaCamposObrigatoriosCobertura = function(target, options) {
	var defaults = {
			fieldsToValidation : undefined,
			validationComplete : undefined,
			validateInvisibleFields: false,
			validateDisabledFields: false
	};
	var settings = $.extend( {}, defaults, options);

	var retorno = false;

	var fieldsWithValidationError = [];
	var callbackEachFieldValidation = function(){
		var idLabel = $(this).attr('label-validation');
		if ((settings.validateInvisibleFields || $(this).is(":visible")) 
				&& !$(this).hasClass("naoValida") 
				&& (settings.validateDisabledFields || !$(this).is(":disabled")) 
				&& (($(this).val() == undefined) || ($(this).val() == null) || ($.trim($(this).val()) == ''))) {
			idLabel = $(this).attr('label-validation');
			$('#'+idLabel).addClass('naoPreenchido');
			if(idLabel == "label-isBasica"){
				$("#isBasica").addClass("redBorder");
			} else if(idLabel == "label-desconto-fidelidade"){
				$("#tipoSeguro").addClass("redBorder");
			} else if(idLabel == "label-tempoExperiencia"){
				$("#tempoExperiencia").addClass("redBorder");
			}
			var proxID =$('#'+idLabel).next().attr('id');
			$("#" + proxID).addClass("redBorder");						
			retorno = true;
			//fieldsWithValidationError.push($(this));
		}else if($("#tipoSeguro").val() == "RENOVACAO_BRADESCO"){
			if((idLabel == "label-houveAlteracaoEmbarcacao") && (($(this).val() == undefined) || ($(this).val() == null) || (!$("#perguntaAltEmbcao209Sim").is(":checked") && !$("#perguntaAltEmbcao209Nao").is(":checked")))) {
				idLabel = $(this).attr('label-validation');
				$("#label-houveAlteracaoEmbarcacao").addClass("naoPreenchido");						
				retorno = true;
				//fieldsWithValidationError.push($(this));
			}else
				if(idLabel == "label-houveAlteracaoEmbarcacao" && ($(this).val() != undefined || $(this).val() != null) && $("#perguntaAltEmbcao209Sim").is(":checked")) {
					var naoPreenchido = false;
					$(".listaPerguntaAltEmbcao").each(function(){
						if($(this).is(":checked")){
							naoPreenchido= true;
						}
					});
					if(!naoPreenchido){
						$("#label-opcoesAlteraEmbarcacao").addClass("naoPreenchido");		
						retorno = true;
					}
					
					
			}else
				if((idLabel == "label-outrosAlteracaoEmbarcacao") && (($(this).val() == undefined) || ($(this).val() == null) || ($.trim($(this).val()) == '')) && $("#perguntaAltEmbcao208").is(":checked")) {
					var proxID =$('#'+idLabel).next().attr('id');
					$("#outrosAlteracaoEmbarcacao").addClass("redBorder");						
					retorno = true;
					//fieldsWithValidationError.push($(this));
				}
		}
		fieldsWithValidationError.push($(this));
		$(this).on('focus', function() {
			$.removeMensagemCamposObrigatorios();
		});
	};

	var fieldsToValidate = null;
	if(settings.fieldsToValidation == undefined){
		fieldsToValidate = $('.validation');
	} else {
		fieldsToValidate = settings.fieldsToValidation;
	}

	fieldsToValidate.each(callbackEachFieldValidation);

	if ((retorno) && ($('#'+target).size() > 0)) {
		$('#'+target).html('Dados obrigat&oacute;rios n&atilde;o foram preenchidos.');		
	}

	if(settings.validationComplete != null) {
		var validationReturn = {
				'hasErrorValidation': retorno,
				'fieldsWithValidationError': fieldsWithValidationError,
				'optionsValidation': settings
		};
		settings.validationComplete(validationReturn);
		return validationReturn;
	} else {
		return retorno;
	}
};
/**
 * 
 */
jQuery.formatNumber = function(valor) {
	//jQuery.browser.msie && 
	if(valor != null) {
		var number = valor;
		return number.toLocaleString("pt-BR");
	} 
	//Nï¿½o funciona para o IE atï¿½ 0 9 
	/*else {
		var formatter = new Intl.NumberFormat('pt-BR', {
			  //style: 'currency',
			  //currency: 'BRL',
			  maximumFractionDigits: 2,
			});
		if(valor != null){
			return formatter.format(valor);
		}*/ else {
			return valor;
		}
};

jQuery.replaceAll = function(str, de, para){
	if(str != undefined){
		var pos = str.indexOf(de);	
	
		while (pos > -1){
			str = str.replace(de, para);
			pos = str.indexOf(de);
		}
	}
	return (str);
};
/**
 * <p>
 * 	Coloca um div por cima da pagina que nao deixa usuario interagir com a pagina
 * </P>
 * @return
 */
jQuery.showOverlay = function(){
	$('#esqueleto').fadeTo('slow',.6); 
	$('#esqueleto').append(' <div id="divOpacity" style = " position : absolute ; top : 0 ; left : 0 ; width :  100% ; height : 100% ; z-index : 2 ; opacity : 0.4 ; filter : alpha ( opacity =  50 ) " ></div> ');

};
/*==================================================================================================*/
/**
 * <p>
 * 	Esconde um div por cima da pagina que nao deixa usuario interagir com a pagina
 * </P> 
 * @return
 */
jQuery.hideOverlay = function(){
	$("#esqueleto").removeAttr("style");
	$("#divOpacity").remove();
};

Number.prototype.formatMoney = function(digitos, moeda, milhar, decimal) {
	digitos = !isNaN(digitos = Math.abs(digitos)) ? digitos : 2;
	moeda = moeda !== undefined ? moeda : "";
	milhar = milhar || ".";
	decimal = decimal || ",";
	var number = this, 
	    negative = number < 0 ? "-" : "",
	    i = parseInt(number = Math.abs(+number || 0).toFixed(digitos), 10) + "",
	    j = (j = i.length) > 3 ? j % 3 : 0;
	return moeda + negative + (j ? i.substr(0, j) + milhar : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + milhar) + (digitos ? decimal + Math.abs(number - i).toFixed(digitos).slice(2) : "");
};

Number.prototype.formatFator = function(digitos, moeda, milhar, decimal) {
	digitos = !isNaN(digitos = Math.abs(digitos)) ? digitos : 4;
	moeda = moeda !== undefined ? moeda : "";
	milhar = milhar || ".";
	decimal = decimal || ",";
	var number = this, 
	    negative = number < 0 ? "-" : "",
	    i = parseInt(number = Math.abs(+number || 0).toFixed(digitos), 10) + "",
	    j = (j = i.length) > 3 ? j % 3 : 0;
	return moeda + negative + (j ? i.substr(0, j) + milhar : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + milhar) + (digitos ? decimal + Math.abs(number - i).toFixed(digitos).slice(2) : "");
};

/**
 * <p>
 * ReÃºne os elementos elegÃ­veis de um form num Ãºnico objeto JSON.
 * </p>
 */
jQuery.converterFormParaJson = function (idFormulario) {
    var jsonData = {};
	var formData = $("#"+idFormulario).serializeArray();
	$.each(formData, function() {
		if (jsonData[this.name]) {	
			if (!jsonData[this.name].push) {	
				jsonData[this.name] = [jsonData[this.name]];
			}
			jsonData[this.name].push(this.value || '');			
		} else {
			jsonData[this.name] = this.value || '';
		}
	});	
	return jsonData;
};

/**
 * <p>
 * FunÃ§Ã£o utilizada para dar comportamento de checkbox para radio button.
 * </p>
 */
jQuery.fn.uncheckableRadio = function() {

    return this.each(function() {
        $(this).mousedown(function() {
            $(this).data('wasChecked', this.checked);
        });

        $(this).click(function() {
            if ($(this).data('wasChecked'))
                this.checked = false;
        });
    });

};

/*
 * --------------------------------------------------------------------
 * jQuery-Plugin - $.download - allows for simple get/post requests for files
 * by Scott Jehl, scott@filamentgroup.com
 * http://www.filamentgroup.com
 * reference article: http://www.filamentgroup.com/lab/jquery_plugin_for_requesting_ajax_like_file_downloads/
 * Copyright (c) 2008 Filament Group, Inc
 * Dual licensed under the MIT (filamentgroup.com/examples/mit-license.txt) and GPL (filamentgroup.com/examples/gpl-license.txt) licenses.
 * --------------------------------------------------------------------
 */
jQuery.download = function(url, data, method){
	//url and data options required
	if( url && data ){ 
		//data can be string of parameters or array/object
		data = typeof data == 'string' ? data : jQuery.param(data);
		//split params into form inputs
		var inputs = '';
		jQuery.each(data.split('&'), function(){ 
			var pair = this.split('=');
			inputs+='<input type="hidden" name="'+ pair[0] +'" value="'+ pair[1] +'" />'; 
		});
		//send request
		jQuery('<form action="'+ url +'" method="'+ (method||'post') +'">'+inputs+'</form>')
		.appendTo('body').submit().remove();
	};
};
