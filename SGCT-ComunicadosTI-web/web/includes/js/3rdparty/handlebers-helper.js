Handlebars.registerHelper('ifCond', function(v1, v2, options) {
	if(v1 === v2) {
		return options.fn(this);
	}
	return options.inverse(this);
});

Handlebars.registerHelper('ifEqStr', function(v1, v2, options) {
	if(''+v1 === ''+v2) {
		return options.fn(this);
	}
	return options.inverse(this);
})

Handlebars.registerHelper('notEndList', function(list, index, options) {
	if ((list.length - 1) == index) {
		return options.inverse(this);
	}
	return options.fn(this);
});

Handlebars.registerHelper("plus", function(value, options)
{
	return parseInt(value) + 1;
});

Handlebars.registerHelper('gt', function(v1, v2, options) {
	if(v1 > v2) {
		return options.fn(this);
	}
	return options.inverse(this);
});

Handlebars.registerHelper('lt', function(v1, v2, options) {
	if(v1 < v2) {
		return options.fn(this);
	}
	return options.inverse(this);
});

Handlebars.registerHelper('diff', function(v1, v2, options) {
	if(v1 != v2) {
		return options.fn(this);
	}
	return options.inverse(this);
});

Handlebars.registerHelper('getSrc', function(value) {
	return value.split(';')[0];
});
Handlebars.registerHelper('getDescr', function(value) {
	return value.split(';')[1];
});