<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="page" uri="http://www.opensymphony.com/sitemesh/page" %>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
		.tooltip-inner {
	  	  max-width: 100%; 
	  	  min-width: 150px; 
		}
		
		.buttonAcompanhamento{
			margin-top:5px; 
			width:40px !important;
		}	
		
		.popover {
		    border: 1px #faebcc;
		    max-width: 500px;
		    width: 500px;
		}
	
		/* Popover Header */
		.popover-title {
		    background-color: #8a6d3b; 
		    color: #FFF; 
		    font-size: 20px;
		    text-align: center;
		}
	
		/* Popover Body */
		.popover-content {
		    background-color: #fcf8e3;
		    font-size: 13px;
		    color: #8a6d3b;
		    padding: 25px;
		}
	</style>

</head>
<body><!-- 
<p class="destaque">Importante, esta funcionalidade � apenas uma
refer�ncia para o desenvolvimento, e deve ser removida da aplica��o.</p>
<p class="destaque">O pacote br.com.bradseg.sgct.comunicadosti.funcao deve ser removido antes de uma entrega de EAR.</p>
<br/>
<p class="destaque">Para testar, preencha com os nomes: 'bsp', ou 'bsad', ou 'fulano' e veja os diferentes comportamentos da aplica��o. Os nomes 'bsp' e 'bsad' geram respectivamente BusinessException e IntegrationException.</p>
<br/>
<table>
	<thead>
		<tr>
			<th>Nome</th><th>Efeito</th>
		</tr>
	</thead>
	<tbody>
		<tr><td>bsp</td><td>Ocorre de forma proposital, um erro de neg�cio na camada de servi�o, mas que ser� tratada pela action e exibida como mensagem de erro. (BusinessException)</td></tr>
		<tr><td>bsad</td><td>Ocorre de forma proposital, um erro de integra��o na camada de servi�o, que n�o ser� tratado por essa action, e ser� redirecionado para uma p�gina de erro. (IntegrationException)</td></tr>
		<tr><td>fulano</td><td>Exibe uma mensagem espec�fica para este nome.</td></tr>
		<tr><td>(qualquer outro nome)</td><td>Exibe uma mensagem padr�o para qualquer outro nome n�o cadastrado.</td></tr>
	</tbody>
</table>-->
<br/>

<s:form action="consultarSaudacao">
	<table width="30%" class="tabela_interna">
		<tr>
			<th colspan="2">Sauda��o</th>
		</tr>
		<tr>
			<td width="120">Nome: <span class="obrigatorio">*</span></td>
			<td><s:textfield name="nome" maxlength="200" size="30" /></td>
		</tr>
	</table>
	<table id="tabela_botoes" cellspacing="0" cellpadding="0" border="0"
		width="30%">
		<tr align="center">
		<td>
			<s:submit value="Consultar" cssClass="margem_botoes button" />
			</td>
		</tr>
	</table>
</s:form>
</body>
</html>